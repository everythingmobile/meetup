//
//  UserAndContacts.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//
import Foundation

class UserAndContacts {
    var userInfo : UserInfo?
    var userContacts = Array<UserInfo>()
    
    init () {
    }
    
    func toDict() -> Dictionary<String,AnyObject> {
        
        var dictionary : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        if let ui = self.userInfo {
            
            dictionary["userInfo"] = ui.toDict()

        }
        
        var dicArr = Array<Dictionary<String,AnyObject>>()

        for (Person: UserInfo) in self.userContacts {
            println(Person.toDict())
            dicArr.append(Person.toDict())
        
        }
        
        dictionary["userContacts"] = dicArr
        
        return dictionary
    }
}
