//
//  verificationViewController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class verificationViewController: UIViewController {
    
    @IBOutlet weak var verificationField: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    var phone: String  = ""
    var flag: Int = 0
    
    var verificationManager  = VerificationManger()
    @IBAction func editingChanged(sender : AnyObject){
        var phoneNumber = self.verificationField.text
        if phoneNumber.isEmpty == true {
            self.verifyButton.enabled = false
        }else{
            self.verifyButton.enabled = true
        }
    }
    
    @IBAction func verifyButton(sender : AnyObject) {
        self.verificationField.resignFirstResponder()
        var verification =  Verification()
        if self.flag == 0 {
            verification.phoneNumber = SessionManager.sharedsessionManager.userSession!.userInfo!.userPhoneNumber
        } else {
            verification.phoneNumber = self.phone
        }
        verification.verificationString = self.verificationField.text;
        self.verifyButton.enabled = false
        verificationManager.verifyUser(verification).responseString { [unowned self](req, res, string, error) -> Void in
            if error == nil{
                if let authToken = string  {
                    if authToken != ""{
                        SessionManager.sharedsessionManager.userSession!.authToken = authToken
                        PersistenceManager.sharedPersistenceManager.persistAuthToken(authToken)
                        println("verified successfully")
                        println(PersistenceManager.sharedPersistenceManager.getAuthToken()!)
                    }else{
                        println("cant verify")
                    }
                }else{
                    println("cant verify")
                }
            }else{
                println(error)
            }
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.verifyButton.enabled = true
            })
            
        }
    }

    func textFieldShouldReturn(textField : UITextField! )-> Bool{
        self.verificationField.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "pushContact" {
            var contactController : ContactsController = segue.destinationViewController as ContactsController
            if self.flag == 0 {
                contactController.phone = SessionManager.sharedsessionManager.userSession!.userInfo!.userPhoneNumber
            } else {
                contactController.phone = self.phone
            }
            contactController.flag = 1
        }
    }
}

