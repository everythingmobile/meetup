//
//  Verification.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import Foundation

class Verification {
    
    internal var phoneNumber : String?
    internal var verificationString : String?
    
    init () {
    }
    
    func toDict () -> Dictionary<String,AnyObject> {
        
        var dict : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        if let val = self.phoneNumber {
            dict["phoneNumber"] = val
        }
        
        if let vstring = self.verificationString {
            dict["verificationString"] = vstring
        }
        
        return dict
    }
}


