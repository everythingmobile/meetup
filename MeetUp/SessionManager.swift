//
//  SessionManager.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import Foundation

class SessionManager {
    
    class var sharedsessionManager : SessionManager {
        
        struct Static {
            static var instance : SessionManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once (&Static.token) {
            
            Static.instance = SessionManager()
            Static.instance!.userSession = UserSession()
        }
            
        return Static.instance!
    }
    
    internal var userSession : UserSession?
    
    func registerUser (userInfo : UserInfo) {
        
        self.userSession?.userInfo = userInfo
        
        request(.POST, "http://ec2-54-179-132-4.ap-southeast-1.compute.amazonaws.com:8080/MeetusServer/useradd", parameters:  userInfo.toDict(), encoding: .URL)
            .responseString {(request, response, string, error) in
                if  error != nil{
                    println(error)
                } else{
                    PersistenceManager.sharedPersistenceManager.persistUserInfo(userInfo)
                }
        }
    }
}