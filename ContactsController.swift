//
//  ContactsController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import AddressBook
import Foundation

class ContactsController: UITableViewController,UITableViewDataSource,UITableViewDelegate {
    
    var rowCount: Int = 5
    var contactList = Array<Contacts>()
    var verificationManager  = VerificationManger()
    var phone : String = ""
    var flag : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveAndPrintContacts();
        verifyFriends();
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        cell.detailTextLabel?.text = "Temp Label"
        return cell
    }
    
    func retrieveAndPrintContacts() {
        
        let status = ABAddressBookGetAuthorizationStatus()
        
        switch status {
            
        case .Denied, .Restricted:
            
            println("User has denied or restricted your app access to Address Book!")
            
        case .Authorized, .NotDetermined:
            
            var error : Unmanaged<CFError>? = nil
            var addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            if addressBook == nil {
                println(error)
                return
            }
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted:Bool, err:CFError!) in
                if granted {
                    var allContacts:CFArray = ABAddressBookCopyArrayOfAllPeople(addressBook)!.takeUnretainedValue()
                    //println(allContacts)
                    
                    for (person: AnyObject) in allContacts.__conversion() {
                        
                        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
                        let name: CFString! = ABRecordCopyCompositeName(person)!.takeUnretainedValue()
                        let phones: ABMultiValueRef =
                        Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue()
                            as NSObject as ABMultiValueRef
                        
                        let countOfPhones = ABMultiValueGetCount(phones)
                        println(countOfPhones)
                        
                        for index in 0..<countOfPhones {
                            let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
                            let phone: String = Unmanaged.fromOpaque(
                                unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as String
                            
                            if let nm = name {
                                print("\(nm) : ")
                                var contact: Contacts = Contacts(name: nm.__conversion(), phoneNumber: self.filterPhoneNumber((phone)))
                                self.contactList.append(contact)
                            }
                            println(self.contactList.count)
                        }
                    }
                    
                } else {
                    println(err)
                    println("User has denied your app access to Address book!")
                }
            }
        }
    }
    
    func filterPhoneNumber(phoneNumber: String) -> String {
        
        var number = phoneNumber;
        
        number = number.stringByReplacingOccurrencesOfString("[^0-9]", withString: "", options: .RegularExpressionSearch, range:nil)
        
        var char: Character = Array(number)[0]
        
        if char == "0" {
            number = (number.substringFromIndex(advance(number.startIndex, 1)))
            number = "91" + number;
        }
        println(number)
        return number;
    }
    
    func verifyFriends() {
        sleep(3)
        var userAndContacts =  UserAndContacts()
        println("Important : ",self.contactList.count)
        if flag == 0 {
            userAndContacts.userInfo = SessionManager.sharedsessionManager.userSession!.userInfo!
        } else {
            userAndContacts.userInfo = UserInfo(userName: "", userPhoneNumber: self.phone, emailId: "", registered: true)
        }

        for (Person : Contacts) in self.contactList {
            var user = UserInfo(userName: "", userPhoneNumber: Person.phoneNumber, emailId: "", registered: false)
            
            userAndContacts.userContacts.append(user)
        }
        
        self.verificationManager.verifyFriends(userAndContacts).responseJSON { [unowned self](req, res, json, error) -> Void in
            if error == nil {
                println(json)
            } else {
                println(error)
            }
        }
    }
}