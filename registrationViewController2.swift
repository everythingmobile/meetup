//
//  registrationViewController2.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class registrationViewController2: UIViewController {
    
    @IBOutlet weak var phoneNumberFT: UITextField!
    @IBOutlet weak var emailFT: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func editingChanged (sender : AnyObject) {
        
        var phoneNumber = self.phoneNumberFT.text
        var email = self.emailFT.text
        
        if phoneNumber.isEmpty == false || email.isEmpty == false {
            self.registerButton.enabled = true
        }
        else {
            self.registerButton.enabled = false
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "push" {
            var verificationController : verificationViewController = segue.destinationViewController as verificationViewController
            verificationController.phone = "91" + self.phoneNumberFT.text
            verificationController.flag = 1
        }
    }
    
}
