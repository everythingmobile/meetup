//
//  Contacts.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//
import Foundation

class Contacts {
    var phoneNumber: String = ""
    var name: String = ""
    
    init (name: String, phoneNumber: String) {
        self.name = name
        self.phoneNumber = phoneNumber
    }
}
