//
//  registrationViewController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class registrationViewController: UIViewController {
    
    @IBOutlet weak var userNameFT: UITextField!
    @IBOutlet weak var phoneNumberFT: UITextField!
    @IBOutlet weak var emailFT: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func editingChanged (sender : AnyObject) {
        
        var phoneNumber = self.phoneNumberFT.text
        var userName = self.userNameFT.text
        var email = self.emailFT.text
        
        if phoneNumber.isEmpty == false && userName.isEmpty == false && email.isEmpty == false {
            self.registerButton.enabled = true
        }
        else {
            self.registerButton.enabled = false
        }
        
    }
    
    @IBAction func registerUser (sender: AnyObject) {
        
        self.phoneNumberFT.text = "91" + self.phoneNumberFT.text
        var phoneNumber : String = self.phoneNumberFT.text!
        var userName : String = self.userNameFT.text
        var email : String = self.emailFT.text
        
        var userInfo = UserInfo (userName: userName,userPhoneNumber: phoneNumber,emailId: email)
        
        SessionManager.sharedsessionManager.registerUser(userInfo)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
}
